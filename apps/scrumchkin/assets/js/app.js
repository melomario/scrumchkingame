import "../css/app.scss"

import "phoenix_html"
import {Socket} from "phoenix"
import NProgress from "nprogress"
import {LiveSocket} from "phoenix_live_view"
import { Sortable, Plugins } from "@shopify/draggable";

let Hooks = {};

Hooks.ProductBacklog = {
    mounted() {
    this.initDraggables();
  },

  updated() {
    this.sortableCard.destroy();
    this.initDraggables();
  },

  initDraggables() {
    this.sortableCard = new Sortable(
      document.querySelectorAll(".product-backlog"),
      {
        draggable: ".draggable-backlog-item",
        mirror: {
          constrainDimensions: true,
          yAxis: false
        },
        swapAnimation: {
          duration: 200,
          easingFunction: "ease-in-out",
          horizontal: true
        },
        plugins: [Plugins.SwapAnimation]
      }
    );

    this.sortableCard.on("sortable:stop", event => {
      const source = event.data.dragEvent.data.source;
      const itemId = parseInt(source.getAttribute("index"));
      const newIndex = parseInt(event.data.newIndex);
      const cardPayload = {
          current_index: itemId,
          new_index: newIndex
      };
      this.pushEvent("change-item-priority", cardPayload);
    });
  }
};

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")

let liveSocket = new LiveSocket("/live", Socket, {hooks: Hooks, params: {_csrf_token: csrfToken}})

window.addEventListener("phx:page-loading-start", info => NProgress.start())
window.addEventListener("phx:page-loading-stop", info => NProgress.done())

liveSocket.connect()

window.liveSocket = liveSocket
