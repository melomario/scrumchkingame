import { Draggable, Plugins } from "@shopify/draggable";

let DevHooks = {
   mounted() {
    this.initDraggables();
  },

  updated() {
    this.draggableCard.destroy();
    this.initDraggables();
  },

  initDraggables() {
    this.draggableCard = new Draggable(
      document.querySelectorAll(".my-cards"),
      {
        draggable: ".dev-team-card",
      }
    );

  this.draggable.on('drag:start', (event) => {
    clientXStart = event.sensorEvent.clientX
    const refRect = el.getBoundingClientRect()
    const leftPos = clientXStart - refRect.x
    rect.style.display = 'block'
    rect.style.left = `${leftPos}px`
  
    console.assert(refRect.x === 50, refRect, `Expected ${refRect.x} to be 50`)
  })

    this.draggableCard.on("drag:stop", event => {
      console.log("SOLTOU")
      const source = event.data.dragEvent.data.source;
      const itemId = parseInt(source.getAttribute("index"));
      const newIndex = parseInt(event.data.newIndex);
      const cardPayload = {
          current_index: itemId,
          new_index: newIndex
      };
      this.pushEvent("change-item-priority", cardPayload);
    });
  }
    
};

export default DevHooks; 

