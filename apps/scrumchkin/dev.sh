#!/bin/sh
tmux new-session -s scrumchkin \; \
rename-window "web" \; \
send 'nvim .' ENTER \; \
split-window -h \; \
resize-pane -R 40 \; \
send 'iex -S mix phx.server' ENTER \; \
split-window -v \; \
send 'htop' ENTER \; \
attach;
