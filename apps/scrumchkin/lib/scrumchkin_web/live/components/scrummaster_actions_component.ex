defmodule ScrumchkinWeb.Components.ScrummasterActionsComponent do
  use Surface.Component

  def render(assigns) do
    ~H"""
      <div :if={{@is_planned}}>
        <button :on-phx-click="draw-card" phx-value-player_id={{@player_id}}> Comprar Carta </button>
        <button :on-phx-click="remove-impediment" phx-value-player_id={{@player_id}}>Remover Impedimento</button>
      </div>

      <div :if={{!@is_planned}}>
        <form :on-phx-submit="plan-sprint">
          <label for="sprint_size"> Sprint Items: </label>
          <input type="number" name="sprint_size" id="sprint_size" maxlength="1">

          <label for="sprint_goal">Sprint Goal Items: </label>
          <input type="number" name="sprint_goal" id="sprint_goal" maxlength="1">

          <button type="submit">
            Start Sprint!
          </button>
        </form>

      </div>
    """
  end
end
