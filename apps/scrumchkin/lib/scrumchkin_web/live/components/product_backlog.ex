defmodule ScrumchkinWeb.Components.ProductBacklogComponent do
  use Surface.Component
  alias ScrumchkinWeb.Components.ProductBacklogItemComponent, as: ProductBacklogItem

  def render(assigns) do
    ~H"""
    <div class="product-backlog-container" phx-hook="ProductBacklog">
      <div class="product-backlog-title">Product Backlog</div>
      <div class="product-backlog">
        <ProductBacklogItem :for={{ {pbi, id} <- Enum.with_index(@product_backlog) }} player={{@player}} item={{pbi}} index={{id}}/>
      </div>
    </div>
    """
  end
end
