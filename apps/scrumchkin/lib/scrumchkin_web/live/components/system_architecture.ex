defmodule ScrumchkinWeb.Components.SystemArchitectureComponent do
  use Surface.Component
  alias ScrumchkinWeb.Components.ProductBacklogItemComponent, as: ProductBacklogItem

  def render(assigns) do
    ~H"""
    <div class="product-backlog-container" phx-hook="SystemArchitecture">
      <div class="product-backlog-title">System Architecture</div>
      <div class="product-backlog">
        <ProductBacklogItem :for={{ {pbi, id} <- Enum.with_index(@architecture) }} player={{@player}} item={{pbi}} index={{id}}/>
      </div>
    </div>
    """
  end
end
