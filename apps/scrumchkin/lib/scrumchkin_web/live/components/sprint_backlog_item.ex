defmodule ScrumchkinWeb.Components.SprintBacklogItemComponent do
  use Surface.Component

  def render(assigns) do
    ~H"""
      <div class="sprint-item" index={{@index}} :on-phx-click="play-card" phx-value-sprint_item={{@index}}>
        <img :if={{@item.goal?}} class="backlog-item-goal" src="/images/sprint_goal.png">
        <img :if={{@item.type == :feature}} class="backlog-item-image" src="/images/cards/feature/small/{{@item.feature.suit}}_{{@item.feature.name}}.jpg">
        <img :if={{@item.type != :feature}} class="backlog-item-image" src="/images/cards/feature/small/technical_debt.jpg">

        <div :if={{@item.original_value > 0}} class="backlog-item-value">Value: {{@item.value}}</div>
        <div :if={{@item.original_value == 0}} class="backlog-item-value">Value: ???</div>
        <div :if={{@item.original_size > 0 && @item.size > 0}} class="backlog-item-size">To Do: {{@item.size}}</div>
          <div :if={{@item.original_size > 0 && @item.size <= 0}} class="backlog-item-done">DONE!</div>
        <div :if={{@item.original_size == 0}} class="backlog-item-size">To Do: ???</div>
      </div>
    """
  end
end
