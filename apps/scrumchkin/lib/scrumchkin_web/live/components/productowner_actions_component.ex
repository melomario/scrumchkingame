defmodule ScrumchkinWeb.Components.ProductOwnerActionsComponent do
  use Surface.Component

  def render(assigns) do
    ~H"""
      <div>
        <button :on-phx-click="discover-feature" phx-value-player_id={{@player_id}}> Descobrir Feature </button>
        <button :on-phx-click="discover-business-value" phx-value-player_id={{@player_id}}> Descobrir Valor de Negócio </button>
      </div>
    """
  end
end
