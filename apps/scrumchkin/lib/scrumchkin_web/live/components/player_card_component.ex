defmodule ScrumchkinWeb.Components.PlayerCardComponent do
  use Surface.Component

  def render(assigns) do
    ~H"""
    <div class="player-card">
      <img src="/images/cards/work/cover_small.jpg" class="card-image" >
      <img src="/images/cards/work/cover_big.jpg" class="player-card-preview">
    </div>
    """
  end
end
