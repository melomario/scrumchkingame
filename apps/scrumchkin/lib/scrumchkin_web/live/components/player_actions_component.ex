defmodule ScrumchkinWeb.Components.PlayerActionsComponent do
  use Surface.Component
  alias ScrumchkinWeb.Components.DevTeamActionsComponent, as: DevTeamActions
  alias ScrumchkinWeb.Components.ScrummasterActionsComponent, as: ScrumMasterActions
  alias ScrumchkinWeb.Components.ProductOwnerActionsComponent, as: ProductOwnerActions

  def render(assigns) do
    ~H"""
      <div class="player-actions" >
        <form :on-phx-submit="add-player" :if={{@current_player == nil}}>

          <label for="name">Seu nome: </label>
          <input type="text" name="name" id="name" maxlength="10">

          <input type="radio" name="role" id="dev" value="dev" checked>
          <label for="dev">Dev Team</label>

          <input type="radio" name="role" id="sm" value="sm">
          <label for="sm">ScrumMaster</label>

          <input type="radio" name="role" id="po" value="po">
          <label for="po">Product Owner</label>

          <br/>

          <button type="submit">
            Entrar!
          </button>
        </form>

        <div :if={{@current_player}}>
          <ProductOwnerActions player_id={{@player_id}} :if={{@is_planned && @current_player.role == :po}}/>
          <DevTeamActions selected_card={{@selected_card}} id="dev-actions" player={{@current_player}} :if={{@is_planned && @current_player.role == :dev}}/>
          <ScrumMasterActions is_planned={{@is_planned}} player_id={{@player_id}} :if={{@current_player.role == :sm}}/>

          <div :if={{!@is_planned && @current_player.role != :sm}}>
            <h2>The ScrumMaster is facilitating the Sprint Planning</h2>
          </div>
        </div>
    </div>
    """
  end
end
