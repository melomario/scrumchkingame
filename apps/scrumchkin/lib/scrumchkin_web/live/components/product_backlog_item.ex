defmodule ScrumchkinWeb.Components.ProductBacklogItemComponent do
  use Surface.Component

  def render(assigns) do
    ~H"""
      <div class={{"backlog-item", "draggable-backlog-item": @player && @player.role == :po}} index={{@index}}>
        <div class="backlog-item-index">{{@index + 1}}</div>
        <img class="backlog-item-image draggable-handle" src="/images/cards/feature/small/{{@item.feature.suit}}_{{@item.feature.name}}.jpg">
        <div :if={{@item.original_value > 0}} class="backlog-item-value">Value: {{@item.value}}</div>
        <div :if={{@item.original_value == 0}} class="backlog-item-value">Value: ???</div>
        <div :if={{@item.original_size > 0}} class="backlog-item-size">To Do: {{@item.size}}</div>
        <div :if={{@item.original_size == 0}} class="backlog-item-size">To Do: ???</div>
      </div>
    """
  end
end
