defmodule ScrumchkinWeb.Components.DevTeamActionsComponent do
  use Surface.Component

  def render(assigns) do
    ~H"""
      <div>
        <div class="player-actions-buttons">
          <button class="button" phx-click="draw-card" phx-value-player_id={{@player.id}}> Comprar Carta </button>
          <button class="button" phx-click="estimate-feature" phx-value-player_id={{@player.id}}> Estimar </button>
        </div>
        
        <div class="my-cards">
          <div :for={{ {card, index} <- Enum.with_index(@player.hand)}}>
            <img phx-click="select-card" phx-value-card="#{index}" class={{"my-cards-image", "my-cards-image-selected": index == @selected_card}} src="/images/cards/work/small/{{card.name}}.jpg">
          </div>
        </div>
      </div>
    """
  end
end
