defmodule ScrumchkinWeb.Components.SprintBacklogComponent do
  use Surface.Component
  alias ScrumchkinWeb.Components.SprintBacklogItemComponent, as: SprintBacklogItem

  def render(assigns) do
    ~H"""
    <div class="sprint-backlog-container">
      <div class="impediment-overlay" :if={{@impediments > 0}}>
        <img :for={{ _card <- 1..@impediments}} class="impediment-card" src="/images/cards/work/impediment.jpg" alt="impediment">
      </div>

      <div class="sprint-backlog-title">Sprint Backlog - Sprint {{@current_sprint}}, Day {{@current_day}}</div>
      <div class="sprint-backlog">
        <SprintBacklogItem :for={{ {pbi, id} <- Enum.with_index(@sprint_backlog) }} item={{pbi}} index={{id}}/>
      </div>
    </div>
    """
  end
end
