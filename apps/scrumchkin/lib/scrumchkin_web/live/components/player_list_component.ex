defmodule ScrumchkinWeb.Components.PlayerListComponent do
  use Surface.Component

  alias ScrumchkinWeb.Components.PlayerComponent

  def render(assigns) do
    ~H"""
    <div class="players-container" x-data="{ open: true }" :class="{'players-container-hidden': !open}">
      <div class="players-toggle" @click="open = !open">
        <img src="/images/icons/back.svg" class="players-toggle-icon">
      </div>
      <div class="players-container-content">
        <h3> Your awesome team </h3>
        <PlayerComponent :for={{ player <- @players }} player={{player}}/>
      </div>
    </div>
    """
  end
end
