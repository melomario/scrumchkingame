defmodule ScrumchkinWeb.Components.GameTableComponent do
  use Surface.Component
  alias ScrumchkinWeb.Components.PlayerActionsComponent, as: PlayerActions
  alias ScrumchkinWeb.Components.ProductBacklogComponent, as: ProductBacklog
  alias ScrumchkinWeb.Components.SprintBacklogComponent, as: SprintBacklog

  def render(assigns) do
    ~H"""
    <div class="game-table">
      <ProductBacklog player={{@current_player}} product_backlog={{@game.product_backlog}}/>
      <SprintBacklog sprint_backlog={{@game.sprint_backlog}} impediments={{@game.impediments}} current_sprint={{@game.current_sprint}} current_day={{@game.current_day}}/>
      <PlayerActions 
        selected_card={{@selected_card}}
        is_planned={{@game.planned?}} current_player={{@current_player}} player_id={{@player_id}}/>
    </div>
    """
  end
end
