defmodule ScrumchkinWeb.Components.PlayerComponent do
  use Surface.Component

  alias ScrumchkinWeb.Components.PlayerCardComponent, as: PlayerCard

  @roles %{
    :sm => "SM",
    :po => "PO",
    :dev => "DT"
  }

  def render(assigns) do
    assigns = Map.put_new(assigns, :role_name, @roles[assigns.player.role])

    ~H"""
      <div class="player-box">
        <div class="player-box-info">
          <img src="https://avatars.dicebear.com/api/bottts/#{@player.name}.svg?mood[]=happy" class="player-avatar role-{{@player.role}}">
          <div class="player-role player-role-{{@player.role}}">{{@role_name}}</div>
        </div>

        <div class="player-card-box">
          <div class="player-name">{{@player.name}} | Actions: {{@player.actions}}</div>
          <div  class="player-cards">
            <PlayerCard :for={{ card <- @player.hand }} card={{card}}/>
          </div>
        </div>
      </div>
    """
  end
end
