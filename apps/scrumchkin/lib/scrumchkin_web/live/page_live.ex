defmodule ScrumchkinWeb.PageLive do
  use ScrumchkinWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, games: GameRegister.list_all())}
  end

  @impl true
  def handle_event("new_game", _, socket) do
    {:ok, game} = GameEngine.create_game()
    game_id = GameRegister.save(game)
    {:noreply, push_redirect(socket, to: Routes.game_path(socket, :index, game_id))}
  end
end
