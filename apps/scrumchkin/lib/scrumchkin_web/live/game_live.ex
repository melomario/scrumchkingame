defmodule ScrumchkinWeb.GameLive do
  use ScrumchkinWeb, :live_view
  use Surface.Component

  alias ScrumchkinWeb.Components.PlayerListComponent, as: PlayerList
  alias ScrumchkinWeb.Components.GameTableComponent, as: GameTable

  @impl true
  def mount(%{"id" => game_id}, session, socket) do
    if connected?(socket), do: Phoenix.PubSub.subscribe(Scrumchkin.PubSub, game_id)

    {_key, game_pid} = GameRegister.get(game_id)
    game_state = GameEngine.get_game_state(game_pid)

    {:ok,
     assign(socket,
       game_id: game_id,
       game_pid: game_pid,
       game: game_state,
       players: game_state.players,
       session_id: session["_csrf_token"],
       selected_card: -1
     )}
  end

  @impl true
  def handle_event("add-player", %{"name" => player_name, "role" => role}, socket) do
    GameEngine.add_player(
      socket.assigns.game_pid,
      socket.assigns.session_id,
      player_name,
      String.to_atom(role)
    )
    |> broadcast(socket)
  end

  def handle_event(
        "plan-sprint",
        %{"sprint_size" => sprint_size, "sprint_goal" => sprint_goal},
        socket
      ) do
    GameEngine.plan_sprint(
      socket.assigns.game_pid,
      socket.assigns.session_id,
      String.to_integer(sprint_size),
      String.to_integer(sprint_goal)
    )
    |> broadcast(socket)
  end

  def handle_event("draw-card", %{"player_id" => player_id}, socket) do
    GameEngine.draw_card(socket.assigns.game_pid, player_id)
    |> broadcast(socket)
  end

  def handle_event("discover-feature", %{"player_id" => player_id}, socket) do
    GameEngine.discover_feature(socket.assigns.game_pid, player_id)
    |> broadcast(socket)
  end

  def handle_event("discover-business-value", %{"player_id" => player_id}, socket) do
    GameEngine.discover_business_value(socket.assigns.game_pid, player_id)
    |> broadcast(socket)
  end

  def handle_event("estimate-feature", %{"player_id" => player_id}, socket) do
    GameEngine.estimate_feature(socket.assigns.game_pid, player_id)
    |> broadcast(socket)
  end

  def handle_event("remove-impediment", %{"player_id" => player_id}, socket) do
    GameEngine.remove_impediment(socket.assigns.game_pid, player_id)
    |> broadcast(socket)
  end

  def handle_event("select-card", %{"card" => card}, socket) do
    if String.to_integer(card) == socket.assigns.selected_card do
      {:noreply, update(socket, :selected_card, fn _item -> -1 end)}
    else
      {:noreply, update(socket, :selected_card, fn _item -> String.to_integer(card) end)}
    end
  end

  def handle_event("play-card", %{"sprint_item" => sprint_item}, socket) do
    GameEngine.play_card(
      socket.assigns.game_pid,
      socket.assigns.session_id,
      socket.assigns.selected_card,
      String.to_integer(sprint_item)
    )
    |> broadcast(socket)
  end

  def handle_event(
        "change-item-priority",
        %{"current_index" => current_index, "new_index" => new_index},
        socket
      ) do
    GameEngine.change_priority(
      socket.assigns.game_pid,
      socket.assigns.session_id,
      current_index,
      new_index
    )
    |> broadcast(socket)
  end

  @impl true
  def handle_info({:update, game_state}, socket) do
    socket =
      assign(socket,
        game: game_state,
        players: game_state.players
      )

    {:noreply, socket}
  end

  def broadcast({:error, message}, socket) do
    {:noreply, put_flash(socket, :error, message)}
  end

  def broadcast(game_state, socket) do
    Phoenix.PubSub.broadcast(Scrumchkin.PubSub, socket.assigns.game_id, {:update, game_state})
    {:noreply, socket}
  end

  def render(assigns) do
    current_player =
      assigns.players
      |> Enum.find(fn player -> player.id == assigns.session_id end)

    ~H"""
    <div class="game-board">
      <GameTable selected_card={{@selected_card}} player_id={{@session_id}} game={{@game}} current_player={{current_player}} />
      <PlayerList players={{@players}} />
    </div>
    """
  end
end
