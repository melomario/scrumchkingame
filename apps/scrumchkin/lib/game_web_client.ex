defmodule GameWebClient do
  @moduledoc """
  Documentation for `GameWebClient`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> GameWebClient.hello()
      :world

  """
  def hello do
    :world
  end
end
