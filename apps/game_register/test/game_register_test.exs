defmodule GameRegisterTest do
  use ExUnit.Case
  doctest GameRegister

  setup _state do
    :ets.delete_all_objects(:scrumchkin)
    :ok
  end

  test "Um registro inserido pode ser obtido novamente com a chave" do
    value = UUID.uuid1()
    key = GameRegister.save(value)
    {_key, result} = GameRegister.get(key)
    assert result == value
  end

  test "Um registro pode ser removido a qualquer momento" do
    value = UUID.uuid1()
    key = GameRegister.save(value)
    GameRegister.delete(key)

    {:error, _} = GameRegister.get(key)
  end

  test "O registro de jogos consegue listar todos os jogos disponíveis" do
    for _index <- 1..10 do
      value = UUID.uuid1()
      GameRegister.save(value)
    end

    games = GameRegister.list_all()

    assert Enum.count(games) == 10
  end
end
