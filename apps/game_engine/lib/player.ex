defmodule GameEngine.Player do
  defstruct id: nil,
            name: "",
            role: :dev,
            hand: [],
            sick_days: 0,
            actions: 2

  def check_constraints(game_state, player_id, player_role) do
    game_state
    |> check_unique_id(player_id)
    |> check_unique_roles(player_role)
  end

  def renew_all_actions(game_state) do
    players =
      game_state.players
      |> Enum.map(&renew_player/1)

    %GameEngine.GameState{game_state | players: players}
  end

  def renew_player(player = %GameEngine.Player{sick_days: sick_days}) when sick_days > 0 do
    player
    |> Map.update!(:actions, fn _actions -> 0 end)
    |> Map.update!(:sick_days, fn current -> current - 1 end)
  end

  def renew_player(player) do
    %GameEngine.Player{
      player
      | actions: 2
    }
  end

  defp check_unique_id(game_state, player_id) do
    game_state.players
    |> Enum.count(&(&1.id == player_id))
    |> unique_id_result(game_state)
  end

  defp check_unique_roles({:error, _message} = error, _role), do: error

  defp check_unique_roles(game_state, :dev), do: game_state

  defp check_unique_roles(game_state, player_role) do
    game_state.players
    |> Enum.count(&(&1.role == player_role))
    |> unique_role_result(game_state, player_role)
  end

  defp unique_id_result(1, game_state), do: game_state

  defp unique_id_result(_more_than_one, _game_state),
    do: {:error, "This player ID is already being used in this game"}

  defp unique_role_result(people_with_role, game_state, _role) when people_with_role <= 1,
    do: game_state

  defp unique_role_result(_more_than_one, _game_state, :sm),
    do: {:error, "The team already has a ScrumMaster"}

  defp unique_role_result(_more_than_one, _game_state, :po),
    do: {:error, "The team already has a Product Owner"}
end
