defmodule GameEngine.DevTeam do
  alias GameEngine.{GameBasics, GameState, Player, ProductBacklog, ProductBacklogItem}

  @technical_debt_penalty 2

  import GameBasics, only: [use_one_action: 1, update_player: 2]

  def play_card(_game_state, %Player{role: role}, _card_played, _sprint_item)
      when role != :dev do
    {:error, "Only the Dev Team can work on the Sprint Backlog"}
  end

  def play_card(_game_state, %Player{actions: 0}, _card_played, _sprint_item) do
    {:error, "You played all your actions for today"}
  end

  def play_card(_game_state, _player, card_played, _sprint_item) when card_played < 0 do
    {:error, "You must select a card to play"}
  end

  def play_card(game_state, player, card_played, sprint_item_index) do
    sprint_has_bugs? = Enum.any?(game_state.sprint_backlog, fn item -> item.type == :bug end)

    sprint_item = Enum.at(game_state.sprint_backlog, sprint_item_index)

    is_forbidden_play? = sprint_has_bugs? && sprint_item.type != :bug

    resolve_played_card(game_state, player, card_played, sprint_item_index, is_forbidden_play?)
  end

  defp resolve_played_card(game_state, player, card_played, sprint_item_index, false) do
    {card, new_hand} = List.pop_at(player.hand, card_played)

    updated_player = %Player{
      player
      | hand: new_hand
    }

    sprint_backlog =
      List.update_at(game_state.sprint_backlog, sprint_item_index, fn item ->
        %ProductBacklogItem{
          item
          | work_cards: [card | item.work_cards],
            size: item.size - card.value
        }
      end)

    game_state = %GameState{
      game_state
      | sprint_backlog: sprint_backlog
    }

    use_one_action(updated_player)
    |> update_player(game_state)
    |> update_finished_items()
  end

  defp resolve_played_card(_game_state, _player, _card_played, _sprint_item_index, true) do
    {:error, "You can't work on features until you solve the bug"}
  end

  def estimate_feature(_game_state, %Player{actions: 0}) do
    {:error, "You played all your actions for today"}
  end

  def estimate_feature(game_state, player) do
    game_state
    |> ProductBacklog.get_next_item_to_estimate()
    |> assign_feature_estimate(game_state, player)
  end

  defp assign_feature_estimate({_any_backlog, nil}, _game_state, _player),
    do: {:error, "There's no feature to estimate"}

  defp assign_feature_estimate({backlog_to_update, item_index}, game_state, player) do
    {card, sizing_deck} = List.pop_at(game_state.sizing_deck, 0)

    technical_debt =
      Enum.count(game_state.sprint_backlog, fn item -> item.type == :technical_debt end)

    modifiers = Enum.map(1..technical_debt, fn _ -> :technical_debt end)

    backlog_list = Map.get(game_state, backlog_to_update)

    updated_backlog =
      List.update_at(backlog_list, item_index, fn item ->
        %{
          item
          | size: card.value + technical_debt * @technical_debt_penalty,
            original_size: card.value,
            modifiers: modifiers
        }
      end)

    game_state =
      %GameState{
        game_state
        | sizing_deck: sizing_deck
      }
      |> Map.update!(backlog_to_update, fn _ -> updated_backlog end)

    use_one_action(player)
    |> update_player(game_state)
  end

  defp update_finished_items(game_state) do
    game_state.sprint_backlog
    |> Enum.filter(fn item -> item.size <= 0 && item.original_size > 0 end)
    |> Enum.reduce(game_state, fn item, updated_game ->
      finish_backlog_item(updated_game, item)
    end)
  end

  defp finish_backlog_item(game_state, finished_item) do
    sprint_backlog = List.delete(game_state.sprint_backlog, finished_item)

    game_state
    |> Map.update!(:sprint_backlog, fn _backlog -> sprint_backlog end)
    |> Map.update!(:current_product, fn product -> [finished_item | product] end)
    |> update_technical_debt(finished_item)
  end

  def update_technical_debt(game_state, %{type: :technical_debt}) do
    game_state
    |> GameBasics.apply_technical_debt(-2)
    |> update_finished_items()
  end

  def update_technical_debt(game_state, _item), do: game_state
end
