defmodule GameEngine.Decks.WorkCard do
  defstruct feature: %{suit: :technical, name: :debt},
            name: :work,
            type: :work,
            original_value: 0,
            value: 0,
            original_size: 0,
            size: 0,
            work_cards: [],
            modifiers: [],
            goal?: false,
            quantity: 0,
            suit: :none,
            resolve: false,
            image: ""
end
