defmodule GameEngine.ProductBacklogItem do
  defstruct feature: %{},
            type: :feature,
            original_value: 0,
            value: 0,
            original_size: 0,
            size: 0,
            work_cards: [],
            modifiers: [],
            goal?: false
end
