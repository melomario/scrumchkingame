defmodule GameEngine.ProductOwner do
  alias GameEngine.{GameBasics, GameState, Player, ProductBacklog, ProductBacklogItem}

  import GameBasics, only: [use_one_action: 1, update_player: 2]

  def change_item_priority(_game_state, %Player{actions: 0}, _item, _new_index) do
    {:error, "You played all your actions for today"}
  end

  def change_item_priority(_game_state, %Player{role: role}, _item, _new_index)
      when role != :po do
    {:error, "Only the PO can reorder the Product Backlog"}
  end

  def change_item_priority(%GameState{planned?: false}, _player, _item, _new_index) do
    {:error, "We're doing a Sprint Planning"}
  end

  def change_item_priority(game_state, _player, same_index, same_index), do: game_state

  def change_item_priority(game_state, player, current_index, new_index) do
    game_state = move_backlog_item(game_state, current_index, new_index)

    player
    |> use_one_action()
    |> update_player(game_state)
  end

  def discover_feature(_game_state, %Player{actions: 0}) do
    {:error, "You played all your actions for today"}
  end

  def discover_feature(game_state, player) do
    {card, feature_deck} = List.pop_at(game_state.feature_deck, 0)

    updated_backlog =
      Enum.concat(game_state.product_backlog, [%ProductBacklogItem{feature: card}])

    game_state = %GameState{
      game_state
      | feature_deck: feature_deck,
        product_backlog: updated_backlog
    }

    player
    |> use_one_action
    |> update_player(game_state)
  end

  def discover_business_value(_game_state, %Player{actions: 0}) do
    {:error, "You played all your actions for today"}
  end

  def discover_business_value(game_state, player) do
    game_state
    |> ProductBacklog.get_next_item_to_value()
    |> assign_feature_value(game_state, player)
  end

  defp assign_feature_value({_any_backlog, nil}, _game_state, _player),
    do: {:error, "There's no feature without value"}

  defp assign_feature_value({backlog_to_update, item_index}, game_state, player) do
    {card, business_value_deck} = List.pop_at(game_state.business_value_deck, 0)

    backlog_list = Map.get(game_state, backlog_to_update)

    updated_backlog =
      List.update_at(backlog_list, item_index, fn item ->
        %ProductBacklogItem{item | value: card.value, original_value: card.value}
      end)

    game_state =
      %GameState{
        game_state
        | business_value_deck: business_value_deck
      }
      |> Map.update!(backlog_to_update, fn _ -> updated_backlog end)

    player
    |> use_one_action
    |> update_player(game_state)
  end

  defp move_backlog_item(game_state, current_index, new_index) when current_index < new_index do
    item_to_move = Enum.at(game_state.product_backlog, current_index)

    product_backlog =
      game_state.product_backlog
      |> List.delete_at(current_index)
      |> List.insert_at(new_index, item_to_move)

    %GameState{
      game_state
      | product_backlog: product_backlog
    }
  end

  defp move_backlog_item(game_state, current_index, new_index) do
    item_to_move = Enum.at(game_state.product_backlog, current_index)

    product_backlog =
      game_state.product_backlog
      |> List.replace_at(current_index, :PLACEHOLDER)
      |> List.insert_at(new_index, item_to_move)
      |> List.delete(:PLACEHOLDER)

    %GameState{
      game_state
      | product_backlog: product_backlog
    }
  end
end
