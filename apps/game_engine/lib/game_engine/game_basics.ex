defmodule GameEngine.GameBasics do
  @sick_day_penalty 2
  alias GameEngine.{GameState, Decks, Player, ProductOwner, DevTeam}

  defdelegate discover_feature(game_state, player), to: GameEngine.ProductOwner
  defdelegate discover_business_value(game_state, player), to: GameEngine.ProductOwner

  defdelegate change_item_priority(game_state, player, current_index, new_index),
    to: GameEngine.ProductOwner

  defdelegate plan_sprint(game_state, player_id, total_items, sprint_goal),
    to: GameEngine.ScrumMaster

  defdelegate remove_impediment(game_state, player_id), to: GameEngine.ScrumMaster

  defdelegate estimate_feature(game_state, player), to: GameEngine.DevTeam
  defdelegate play_card(game_state, player, card_played, sprint_item), to: GameEngine.DevTeam

  def create_game do
    %GameState{}
    |> Map.replace!(:work_deck, Decks.Work.new_deck())
    |> Map.replace!(:feedback_deck, Decks.Feedback.new_deck())
    |> Map.replace!(:sizing_deck, Decks.Sizing.new_deck())
    |> Map.replace!(:business_value_deck, Decks.BusinessValue.new_deck())
    |> Map.replace!(:feature_deck, Decks.Feature.new_deck())
    |> repeat(5, fn game_state -> ProductOwner.discover_feature(game_state, :GAME_MASTER) end)
    |> repeat(2, fn game_state -> DevTeam.estimate_feature(game_state, :GAME_MASTER) end)
    |> repeat(3, fn game_state ->
      ProductOwner.discover_business_value(game_state, :GAME_MASTER)
    end)
  end

  def draw_card(_game_state, %Player{actions: 0}) do
    {:error, "You played all your actions for today"}
  end

  def draw_card(game_state, player) do
    {card, work_deck} = List.pop_at(game_state.work_deck, 0)
    game_state = %GameState{game_state | work_deck: work_deck}

    resolve_card_draw(game_state, player, card)
  end

  def add_player(%GameState{} = game_state, player_id, player_name, player_role) do
    player = %Player{name: player_name, role: player_role, id: player_id}

    game_state
    |> Map.replace!(:players, [player | game_state.players])
    |> Player.check_constraints(player_id, player_role)
  end

  def use_one_action(:GAME_MASTER), do: :GAME_MASTER

  def use_one_action(player) do
    %Player{player | actions: player.actions - 1}
  end

  def update_player(:GAME_MASTER, game_state), do: game_state

  def update_player(player, game_state) do
    player_index = Enum.find_index(game_state.players, &(&1.id == player.id))

    player_list =
      game_state.players
      |> List.replace_at(player_index, player)

    game_state
    |> Map.replace!(:players, player_list)
    |> update_calendar()
  end

  defp resolve_card_draw(game_state, player, %{type: :impediment}) do
    player
    |> use_one_action()
    |> update_player(game_state)
    |> Map.update!(:impediments, fn impediments -> impediments + 1 end)
  end

  defp resolve_card_draw(game_state, player, %{type: :sick_day}) do
    player
    |> Map.replace!(:actions, 0)
    |> Map.replace!(:sick_days, @sick_day_penalty)
    |> update_player(game_state)
  end

  defp resolve_card_draw(game_state, player, bug_card = %{name: :bug}) do
    player
    |> use_one_action
    |> update_player(game_state)
    |> Map.update!(:sprint_backlog, fn backlog -> [bug_card | backlog] end)
  end

  defp resolve_card_draw(game_state, player, debt = %{type: :technical_debt}) do
    player
    |> use_one_action
    |> update_player(game_state)
    |> apply_technical_debt()
    |> Map.update!(:sprint_backlog, fn backlog -> backlog ++ [debt] end)
  end

  defp resolve_card_draw(game_state, player, card) do
    player
    |> Map.update!(:hand, fn hand -> [card | hand] end)
    |> use_one_action()
    |> update_player(game_state)
  end

  defp repeat(game_state, times, execute) do
    Enum.reduce(1..times, game_state, fn _index, state -> execute.(state) end)
  end

  defp update_calendar(game_state) do
    actions_left =
      game_state.players
      |> Enum.reduce(0, fn player, total_actions -> total_actions + player.actions end)

    update_day_and_sprint(game_state, actions_left)
  end

  defp update_day_and_sprint(game_state = %GameState{current_day: 5}, 0) do
    game_state
    |> Map.update!(:current_sprint, fn current_sprint -> current_sprint + 1 end)
    |> Map.update!(:current_day, fn _day -> 1 end)
    |> Player.renew_all_actions()
  end

  defp update_day_and_sprint(game_state, 0) do
    game_state
    |> Map.update!(:current_day, fn current_day -> current_day + 1 end)
    |> Player.renew_all_actions()
  end

  defp update_day_and_sprint(game_state, _actions_left), do: game_state

  def apply_technical_debt(game_state), do: apply_technical_debt(game_state, 2)

  def apply_technical_debt(game_state, debt_to_apply) when debt_to_apply > 0 do
    product_backlog =
      game_state.product_backlog
      |> Enum.map(fn item -> %{item | size: item.size + debt_to_apply} end)
      |> Enum.map(fn item -> %{item | modifiers: [:technical_debt | item.modifiers]} end)

    sprint_backlog =
      game_state.sprint_backlog
      |> Enum.map(fn item -> %{item | size: item.size + debt_to_apply} end)
      |> Enum.map(fn item -> %{item | modifiers: [:technical_debt | item.modifiers]} end)

    %GameState{
      game_state
      | sprint_backlog: sprint_backlog,
        product_backlog: product_backlog
    }
  end

  def apply_technical_debt(game_state, debt_to_apply) when debt_to_apply < 0 do
    product_backlog =
      game_state.product_backlog
      |> Enum.map(fn item -> %{item | size: item.size + debt_to_apply} end)
      |> Enum.map(fn item -> %{item | modifiers: List.delete(item.modifiers, :technical_debt)} end)

    sprint_backlog =
      game_state.sprint_backlog
      |> Enum.map(fn item -> %{item | size: item.size + debt_to_apply} end)
      |> Enum.map(fn item -> %{item | modifiers: List.delete(item.modifiers, :technical_debt)} end)

    %GameState{
      game_state
      | sprint_backlog: sprint_backlog,
        product_backlog: product_backlog
    }
  end
end
