defmodule GameEngine.ScrumMaster do
  alias GameEngine.{GameBasics, GameState, Player, ProductBacklogItem}
  import GameBasics, only: [use_one_action: 1, update_player: 2]

  def remove_impediment(%GameState{impediments: 0}, _player) do
    {:error, "There's no impediment to remove"}
  end

  def remove_impediment(_game_state, %Player{actions: 0}) do
    {:error, "You don't have any actions left"}
  end

  def remove_impediment(game_state, player) do
    game_state = Map.update!(game_state, :impediments, &(&1 - 1))

    player
    |> use_one_action
    |> update_player(game_state)
  end

  def plan_sprint(_game_state, %Player{role: role}, _sprint_items, _sprint_goal)
      when role != :sm,
      do: {:error, "The ScrumMaster should facilitate the Sprint Planning"}

  def plan_sprint(_game_state, _player, sprint_items, sprint_goal)
      when sprint_goal > sprint_items,
      do: {:error, "The Sprint Goal should be a subset of the Sprint Backlog"}

  def plan_sprint(%GameState{planned?: true}, _player, _sprint_items, _sprint_goal),
    do: {:error, "This Sprint is already planned"}

  def plan_sprint(
        %GameState{product_backlog: product_backlog},
        _player,
        sprint_items,
        _sprint_goal
      )
      when length(product_backlog) < sprint_items,
      do: {:error, "Your Product Backlog doesn't have that many items"}

  def plan_sprint(game_state, _player, sprint_items, sprint_goal) do
    {sprint_backlog, product_backlog} = Enum.split(game_state.product_backlog, sprint_items)

    sprint_backlog =
      sprint_backlog
      |> Enum.with_index(1)
      |> Enum.map(fn {item, index} -> %ProductBacklogItem{item | goal?: index <= sprint_goal} end)

    game_state = %GameState{
      game_state
      | product_backlog: product_backlog,
        sprint_backlog: sprint_backlog,
        planned?: true
    }

    game_state
  end
end
