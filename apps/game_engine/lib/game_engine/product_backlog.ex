defmodule GameEngine.ProductBacklog do
  def get_next_item_to_value(game_state) do
    pending_sprint_values =
      game_state.sprint_backlog
      |> Enum.count(fn item ->
        item.original_value == 0
      end)

    backlog_to_update =
      cond do
        pending_sprint_values > 0 -> :sprint_backlog
        true -> :product_backlog
      end

    index =
      game_state
      |> Map.get(backlog_to_update)
      |> Enum.find_index(fn item -> item.original_value == 0 end)

    {backlog_to_update, index}
  end

  def get_next_item_to_estimate(game_state) do
    pending_sprint_estimates =
      game_state.sprint_backlog
      |> Enum.count(fn item ->
        item.size == 0
      end)

    backlog_to_update =
      cond do
        pending_sprint_estimates > 0 -> :sprint_backlog
        true -> :product_backlog
      end

    index =
      game_state
      |> Map.get(backlog_to_update)
      |> Enum.find_index(fn item -> item.size == 0 end)

    {backlog_to_update, index}
  end
end
