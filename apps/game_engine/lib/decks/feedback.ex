defmodule GameEngine.Decks.Feedback do
  @cards [
    %{name: :good_skip, quantity: 1, value: 5, image: "", resolve: true},
    %{name: :bad_ritchie, quantity: 1, value: -5, image: "", resolve: true},
    %{name: :hurry_up, quantity: 1, value: -80, image: "", resolve: true},
    %{name: :ok_im_in, quantity: 1, value: 0, image: "", resolve: true},
    %{name: :go_mobile, quantity: 1, value: 0, image: "", resolve: true},
    %{name: :spam, quantity: 1, value: -20, image: "", resolve: true},
    %{name: :treasure_map, quantity: 1, value: 15, image: "", resolve: true}
  ]

  def new_deck() do
    @cards
    |> GameEngine.Decks.Shuffler.unwind_deck()
  end
end
