defmodule GameEngine.Decks.Shuffler do
  def unwind_deck(deck) do
    deck
    |> Enum.map(fn card -> List.duplicate(card, card.quantity) end)
    |> List.flatten()
    |> Enum.shuffle()
  end
end
