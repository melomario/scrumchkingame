defmodule GameEngine.Decks.Feature do
  @cards [
    %{name: :login, quantity: 1, suit: :turing, image: "", tag: :login},
    %{name: :maps, quantity: 1, suit: :turing, image: "", tag: :maps},
    %{name: :integration, quantity: 1, suit: :turing, image: "", tag: :integration},
    %{name: :upload, quantity: 1, suit: :turing, image: ""},
    %{name: :export, quantity: 1, suit: :turing, image: ""},
    %{name: :crud, quantity: 1, suit: :turing, image: ""},
    %{name: :chart, quantity: 1, suit: :turing, image: ""},
    %{name: :notification, quantity: 1, suit: :turing, image: ""},
    %{name: :performance, quantity: 1, suit: :turing, image: ""},
    %{name: :responsivity, quantity: 1, suit: :turing, image: ""},
    %{name: :gamification, quantity: 1, suit: :turing, image: ""}
  ]

  def new_deck() do
    @cards
    |> add_suit(:ritchie)
    |> add_suit(:skip)
    |> add_suit(:lovelace)
    |> GameEngine.Decks.Shuffler.unwind_deck()
  end

  defp add_suit(cards, suit) do
    @cards
    |> Enum.map(fn card -> %{card | suit: suit} end)
    |> Enum.concat(cards)
    |> List.flatten()
  end
end
