defmodule GameEngine.Decks.Sizing do
  @cards [
    %{name: :size_1, quantity: 2, value: 1, image: ""},
    %{name: :size_2, quantity: 4, value: 2, image: ""},
    %{name: :size_3, quantity: 8, value: 3, image: ""},
    %{name: :size_5, quantity: 16, value: 5, image: ""},
    %{name: :size_8, quantity: 8, value: 8, image: ""},
    %{name: :size_13, quantity: 4, value: 13, image: ""},
    %{name: :size_21, quantity: 2, value: 21, image: ""}
  ]

  def new_deck() do
    @cards
    |> GameEngine.Decks.Shuffler.unwind_deck()
  end
end
