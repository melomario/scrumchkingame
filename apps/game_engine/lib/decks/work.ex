defmodule GameEngine.Decks.Work do
  alias GameEngine.Decks.WorkCard

  @cards [
    %WorkCard{
      name: :bug,
      type: :bug,
      quantity: 8,
      original_size: 5,
      size: 5,
      suit: :none,
      resolve: true
    },
    %WorkCard{name: :sick_day, type: :sick_day, quantity: 4, resolve: true},
    %WorkCard{name: :impediment, quantity: 4, image: "", resolve: true, value: 0},
    %WorkCard{
      name: :technical_debt,
      type: :technical_debt,
      quantity: 4,
      size: 10,
      original_size: 10,
      suit: :none,
      value: 0,
      resolve: true
    },
    %WorkCard{name: :productive_day, quantity: 4, image: ""},
    %WorkCard{name: :continuous_integration, quantity: 4, image: ""},
    %WorkCard{name: :automated_tests, quantity: 4, image: ""},
    %WorkCard{name: :work_1, quantity: 32, value: 1, image: ""},
    %WorkCard{name: :work_2, quantity: 16, value: 2, image: ""},
    %WorkCard{name: :work_3, quantity: 4, value: 3, image: ""}
  ]

  def new_deck() do
    @cards
    |> GameEngine.Decks.Shuffler.unwind_deck()
  end
end
