defmodule GameEngine.Decks.BusinessValue do
  @cards [
    %{name: :bv_100, quantity: 2, value: 100, image: ""},
    %{name: :bv_50, quantity: 5, value: 50, image: ""},
    %{name: :bv_20, quantity: 8, value: 20, image: ""},
    %{name: :bv_10, quantity: 13, value: 10, image: ""},
    %{name: :bv_5, quantity: 8, value: 5, image: ""},
    %{name: :bv_1, quantity: 8, value: 1, image: ""}
  ]

  def new_deck() do
    @cards
    |> GameEngine.Decks.Shuffler.unwind_deck()
  end
end
