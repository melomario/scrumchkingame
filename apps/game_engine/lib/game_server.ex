defmodule GameEngine.GameServer do
  use GenServer

  alias GameEngine.GameBasics, as: Engine
  alias GameEngine.GameState

  @game_timeout 48 * 60 * 60 * 1000

  def start_link(_args) do
    GenServer.start_link(__MODULE__, nil)
  end

  def init(_) do
    {:ok, Engine.create_game(), @game_timeout}
  end

  def handle_info(:timeout, _socket) do
    {:stop, "Não houve nenhuma jogada no jogo nas últimas 48 horas"}
  end

  def handle_call(:game_state, _from, state) do
    format_response(state, state)
  end

  def handle_call({:add_player, player_id, player_name, player_role}, _from, game_state) do
    Engine.add_player(game_state, player_id, player_name, player_role)
    |> format_response(game_state)
  end

  def handle_call({:plan_sprint, player_id, total_items, sprint_goal}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.plan_sprint(game_state, player, total_items, sprint_goal)
    |> format_response(game_state)
  end

  def handle_call({:draw_card, player_id}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.draw_card(game_state, player)
    |> format_response(game_state)
  end

  def handle_call({:remove_impediment, player_id}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.remove_impediment(game_state, player)
    |> format_response(game_state)
  end

  def handle_call({:play_card, player_id, card_played, sprint_item}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.play_card(game_state, player, card_played, sprint_item)
    |> format_response(game_state)
  end

  def handle_call({:discover_feature, player_id}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.discover_feature(game_state, player)
    |> format_response(game_state)
  end

  def handle_call({:discover_business_value, player_id}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.discover_business_value(game_state, player)
    |> format_response(game_state)
  end

  def handle_call({:estimate_feature, player_id}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.estimate_feature(game_state, player)
    |> format_response(game_state)
  end

  def handle_call({:change_priority, player_id, current_index, new_index}, _from, game_state) do
    player = get_player(game_state, player_id)

    Engine.change_item_priority(game_state, player, current_index, new_index)
    |> format_response(game_state)
  end

  defp get_player(game_state, player_id) do
    game_state.players
    |> Enum.find(fn player -> player.id == player_id end)
  end

  defp format_response({:error, message}, state) do
    {:reply, {:error, message}, state, @game_timeout}
  end

  defp format_response(new_state, _state) do
    {:reply, GameState.get_summary(new_state), new_state, @game_timeout}
  end
end
