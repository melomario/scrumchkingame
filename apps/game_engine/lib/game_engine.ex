defmodule GameEngine do
  alias GameEngine.GameServer

  def create_game do
    DynamicSupervisor.start_child(GameEngine.ScrumchkinGamesSupervisor, GameServer)
  end

  def get_game_state(game_pid) do
    GenServer.call(game_pid, :game_state)
  end

  def add_player(game_pid, player_id, player_name, player_role) do
    GenServer.call(game_pid, {:add_player, player_id, player_name, player_role})
  end

  def draw_card(game_pid, player_id) do
    GenServer.call(game_pid, {:draw_card, player_id})
  end

  def discover_feature(game_pid, player_id) do
    GenServer.call(game_pid, {:discover_feature, player_id})
  end

  def discover_business_value(game_pid, player_id) do
    GenServer.call(game_pid, {:discover_business_value, player_id})
  end

  def estimate_feature(game_pid, player_id) do
    GenServer.call(game_pid, {:estimate_feature, player_id})
  end

  def change_priority(game_pid, player_id, current_index, new_index) do
    GenServer.call(game_pid, {:change_priority, player_id, current_index, new_index})
  end

  def plan_sprint(game_pid, player_id, total_items, sprint_goal) do
    GenServer.call(game_pid, {:plan_sprint, player_id, total_items, sprint_goal})
  end

  def play_card(game_pid, player_id, card_played, sprint_item) do
    GenServer.call(game_pid, {:play_card, player_id, card_played, sprint_item})
  end

  def remove_impediment(game_pid, player_id) do
    GenServer.call(game_pid, {:remove_impediment, player_id})
  end

  defdelegate gather_feedback, to: GameEngine.Engine
  defdelegate give_card, to: GameEngine.Engine
  defdelegate move_card, to: GameEngine.Engine
end
