defmodule GameEngine.GameState do
  defstruct players: [],
            product_backlog: [],
            sprint_backlog: [],
            feedbacks: [],
            feedback_deck: [],
            work_deck: [],
            business_value_deck: [],
            sizing_deck: [],
            feature_deck: [],
            current_day: 1,
            current_sprint: 1,
            current_product: [],
            architecture: [],
            planned?: false,
            impediments: 0

  def get_summary(game_state) do
    %{
      game_state
      | work_deck: Enum.count(game_state.work_deck),
        business_value_deck: Enum.count(game_state.business_value_deck),
        sizing_deck: Enum.count(game_state.sizing_deck),
        feature_deck: Enum.count(game_state.feature_deck),
        feedback_deck: Enum.count(game_state.feedback_deck)
    }
  end
end
