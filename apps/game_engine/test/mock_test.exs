defmodule MockTest do
  alias GameEngine.{Player}

  def get_player(options) do
    %Player{
      name: "Teste",
      role: :dev,
      hand: [],
      id: "#{:random.uniform()}"
    }
    |> Map.merge(options)
  end
end
