defmodule GameEngineTest do
  use ExUnit.Case
  doctest GameEngine

  alias GameEngine.{GameState, GameBasics, Player}

  @work_deck_size 84
  @sizing_deck_size 44 - 2
  @feature_deck_size 44 - 5
  @business_value_deck_size 44 - 3
  @feedback_deck_size 7

  setup do
    scrummaster = %Player{id: :sm, name: "Mario", actions: 2, role: :sm}
    productowner = %Player{id: :po, name: "Alysson", actions: 2, role: :po}
    dev1 = %Player{id: :dev1, name: "Donato", actions: 2, role: :dev}
    players = [scrummaster, productowner, dev1]

    game_state = %GameState{players: players}

    {:ok, game_state: game_state}
  end

  test "new game has all the cards ready" do
    new_game = GameBasics.create_game()
    assert %GameState{} = new_game
    assert length(new_game.work_deck) == @work_deck_size
    assert length(new_game.feedback_deck) == @feedback_deck_size
    assert length(new_game.sizing_deck) == @sizing_deck_size
    assert length(new_game.business_value_deck) == @business_value_deck_size
    assert length(new_game.feature_deck) == @feature_deck_size
  end

  test "the game has an initial product backlog" do
    game = GameBasics.create_game()

    features_with_value =
      game.product_backlog
      |> Enum.count(&(&1.value > 0))

    features_with_size =
      game.product_backlog
      |> Enum.count(&(&1.size > 0))

    assert length(game.product_backlog) == 5
    assert features_with_value == 3
    assert features_with_size == 2
  end

  test "a player can join the game", state do
    game_state = state[:game_state]

    assert Enum.count(game_state.players) == 3

    Enum.each(game_state.players, fn player ->
      assert player.actions == 2
    end)
  end

  test "each player has an unique id", state do
    game_state = state[:game_state]

    result = GameBasics.add_player(game_state, :sm, "mario", :sm)

    assert result == {:error, "This player ID is already being used in this game"}
  end

  test "a player with available actions can draw a card", state do
    game_state = %GameState{
      state[:game_state]
      | work_deck: [%{name: :work_1}, %{name: :work_2}]
    }

    player = Enum.find(game_state.players, fn player -> player.id == :sm end)

    game_state =
      game_state
      |> GameBasics.draw_card(player)

    updated_player = Enum.find(game_state.players, fn player -> player.id == :sm end)

    assert updated_player.actions == player.actions - 1
  end

  test "a day ends when the team spent all its actions, and everyone gain 2 more actions" do
    player = %Player{actions: 1, id: 1}
    player2 = %Player{actions: 0, id: 2}
    player3 = %Player{actions: 0, id: 3}

    game_state =
      %GameState{current_day: 2, current_sprint: 1, players: [player, player2, player3]}
      |> GameBasics.draw_card(player)

    player2 = Enum.at(game_state.players, 1)
    player3 = Enum.at(game_state.players, 2)

    assert game_state.current_day == 3
    assert player2.actions == 2
    assert player3.actions == 2
  end

  test "a sprint ends after its 5th day" do
    player = %Player{actions: 1, id: 1}
    player2 = %Player{actions: 0, id: 2}
    player3 = %Player{actions: 0, id: 3}

    game_state =
      %GameState{current_day: 5, current_sprint: 1, players: [player, player2, player3]}
      |> GameBasics.draw_card(player)

    player2 = Enum.at(game_state.players, 1)
    player3 = Enum.at(game_state.players, 2)

    assert game_state.current_day == 1
    assert game_state.current_sprint == 2
    assert player2.actions == 2
    assert player3.actions == 2
  end

  test "a player with no available actions can not draw a card" do
    player = %Player{actions: 0, id: 1}

    game_state =
      %GameState{players: [player]}
      |> GameBasics.draw_card(player)

    assert {:error, _} = game_state
  end

  test "when a player draws a card, it goes to his/her hand", state do
    game_state = %{
      state[:game_state]
      | work_deck: [%{name: :work_1}]
    }

    player = Enum.at(game_state.players, 1)

    game_state = GameBasics.draw_card(game_state, player)
    player = Enum.at(game_state.players, 1)

    assert Enum.count(player.hand) == 1
    assert length(game_state.work_deck) == 0
  end

  test "When a team already has a ScrumMaster, you can't join as a SM", state do
    game_state = state[:game_state]

    result = GameBasics.add_player(game_state, "new_sm", "new_sm", :sm)

    assert result == {:error, "The team already has a ScrumMaster"}
  end

  test "When a team already has a Product Owner, you can't join as a PO", state do
    game_state = state[:game_state]

    result = GameBasics.add_player(game_state, "new_po", "new_po", :po)

    assert result == {:error, "The team already has a Product Owner"}
  end

  test "an impediment automatically comes into play" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: []}

    game_state =
      %GameState{players: [player], work_deck: [%{type: :impediment, resolve: true}]}
      |> GameBasics.draw_card(player)

    updated_player = Enum.at(game_state.players, 0)

    assert length(player.hand) == length(updated_player.hand)
    assert length(game_state.work_deck) == 0
    assert game_state.impediments == 1
  end

  test "a player can get sick and lose 2 days of work" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: []}
    player2 = %Player{id: 1234, role: :dev, actions: 2, hand: []}

    game_state =
      %GameState{players: [player, player2], work_deck: [%{type: :sick_day, resolve: true}]}
      |> GameBasics.draw_card(player)

    updated_player = Enum.at(game_state.players, 0)

    assert length(player.hand) == length(updated_player.hand)
    assert length(game_state.work_deck) == 0
    assert updated_player.actions == 0
    assert updated_player.sick_days == 2
  end

  test "a sick player doesn't receive any new actions for the next day" do
    player = %Player{actions: 1, id: 1}
    player2 = %Player{actions: 0, id: 2, sick_days: 2}

    game_state =
      %GameState{current_day: 2, current_sprint: 1, players: [player, player2]}
      |> GameBasics.draw_card(player)

    player2 = Enum.at(game_state.players, 1)

    assert game_state.current_day == 3
    assert player2.actions == 0
    assert player2.sick_days == 1
  end
end
