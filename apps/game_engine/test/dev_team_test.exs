defmodule DevTeamTest do
  use ExUnit.Case

  alias GameEngine.{DevTeam, GameBasics, GameState, Player, ProductBacklogItem}

  @sizing_deck_size 44 - 2

  setup do
    {:ok, game_pid} = GameEngine.create_game()
    GameEngine.add_player(game_pid, "mario", "mario", :sm)
    GameEngine.add_player(game_pid, "alysson", "alysson", :po)
    GameEngine.add_player(game_pid, "jacque", "jacque", :dev)
    GameEngine.add_player(game_pid, "chris", "chris", :dev)

    GameEngine.add_player(game_pid, "marcelo", "marcelo", :dev)

    {:ok, game_state: GameEngine.get_game_state(game_pid), game_pid: game_pid}
  end

  test "the dev team can estimate a feature", state do
    game_pid = state[:game_pid]
    game_state = state[:game_state]
    player_id = Enum.at(game_state.players, 0).id

    game_state = GameEngine.discover_feature(game_pid, player_id)

    first_unestimated_item_index =
      Enum.find_index(game_state.product_backlog, fn item -> item.size == 0 end)

    game_state = GameEngine.estimate_feature(game_pid, player_id)

    feature = Enum.at(game_state.product_backlog, first_unestimated_item_index)

    assert feature.size > 0
    assert feature.original_size > 0
    assert game_state.sizing_deck == @sizing_deck_size - 1
  end

  test "the dev team can't estimate if there's no feature to estimate" do
    game_state =
      %GameState{product_backlog: []}
      |> DevTeam.estimate_feature(:GAME_MASTER)

    assert {:error, "There's no feature to estimate"} = game_state
  end

  test "a dev team member must select a card to play" do
    cards = [%{name: :work_2, quantity: 16, value: 2, image: ""}]
    player = %Player{role: :dev, actions: 1, id: 123, name: "teste", hand: cards}
    sprint_backlog = [%ProductBacklogItem{size: 8}, %ProductBacklogItem{size: 5}]

    card_played = -1
    sprint_item = 0

    game_state =
      %GameState{players: [player], sprint_backlog: sprint_backlog}
      |> DevTeam.play_card(player, card_played, sprint_item)

    assert {:error, "You must select a card to play"} = game_state
  end

  test "the dev team can't play cards if there's no available actions" do
    player = %Player{role: :dev, actions: 0, id: 123, name: "teste"}
    card_played = 0
    sprint_item = 0

    game_state =
      %GameState{players: [player], sprint_backlog: [:item1, :item2]}
      |> DevTeam.play_card(player, card_played, sprint_item)

    assert {:error, "You played all your actions for today"} = game_state
  end

  test "only the dev team can work on the Sprint Backlog" do
    player = %Player{role: :sm, actions: 1, id: 123, name: "teste"}
    card_played = 0
    sprint_item = 0

    game_state =
      %GameState{players: [player], sprint_backlog: [:item1, :item2]}
      |> DevTeam.play_card(player, card_played, sprint_item)

    assert {:error, "Only the Dev Team can work on the Sprint Backlog"} = game_state
  end

  test "a Dev Team member can work on a Sprint Backlog Feature" do
    cards = [%{name: :work_2, quantity: 16, value: 2, image: ""}]
    player = %Player{role: :dev, actions: 1, id: 123, name: "teste", hand: cards}

    sprint_backlog = [
      %ProductBacklogItem{size: 8, feature: %ProductBacklogItem{}},
      %ProductBacklogItem{size: 5, feature: %ProductBacklogItem{}}
    ]

    card_played = 0
    sprint_item = 1

    game_state =
      %GameState{players: [player], sprint_backlog: sprint_backlog}
      |> DevTeam.play_card(player, card_played, sprint_item)

    worked_item = Enum.at(game_state.sprint_backlog, sprint_item)
    updated_player = Enum.at(game_state.players, 0)

    assert length(worked_item.work_cards) == 1
    assert worked_item.size == 5 - 2
    assert length(updated_player.hand) == length(player.hand) - 1
  end

  test "a player can't work on a feature if there's a bug to fix" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: [%{name: :work_2, value: 1}]}
    player2 = %Player{id: 1234, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %ProductBacklogItem{type: :bug, size: 5},
      %ProductBacklogItem{feature: %{name: :maps}, size: 3, value: 10}
    ]

    game_state =
      %GameState{players: [player, player2], sprint_backlog: sprint_backlog}
      |> GameBasics.play_card(player, 0, 1)

    assert {:error, "You can't work on features until you solve the bug"} == game_state
  end

  test "a finished item should leave the Sprint Backlog" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: [%{name: :work_2, value: 2}]}
    player2 = %Player{id: 1234, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %ProductBacklogItem{feature: %{name: :maps}, size: 1, original_size: 1, value: 10}
    ]

    game_state =
      %GameState{players: [player, player2], sprint_backlog: sprint_backlog}
      |> GameBasics.play_card(player, 0, 0)

    assert length(game_state.sprint_backlog) == 0
    assert length(game_state.current_product) == 1
  end

  test "when Technical Debt is killed, all items should be updated" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: [%{name: :work_2, value: 2}]}
    player2 = %Player{id: 1234, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %ProductBacklogItem{type: :technical_debt, size: 1, original_size: 5},
      %ProductBacklogItem{feature: %{name: :integration}, size: 7, value: 10, original_size: 5},
      %ProductBacklogItem{feature: %{name: :maps}, size: 1, value: 5, original_size: 5},
      %ProductBacklogItem{feature: %{name: :notification}, size: 2, value: 100, original_size: 5}
    ]

    product_backlog = [
      %ProductBacklogItem{feature: %{name: :notification}, size: 5, value: 100, original_size: 5}
    ]

    game_state =
      %GameState{
        players: [player, player2],
        sprint_backlog: sprint_backlog,
        product_backlog: product_backlog
      }
      |> GameBasics.play_card(player, 0, 0)

    updated_pbi = Enum.at(game_state.product_backlog, 0)
    sprint_item = Enum.at(game_state.sprint_backlog, 0)

    assert length(game_state.sprint_backlog) == 1
    assert updated_pbi.size == 3
    assert sprint_item.size == 5
    assert length(sprint_item.modifiers) == 0
  end

  test "a player can work on a bug if there's more than one bug to fix" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: [%{name: :work_2, value: 1}]}
    player2 = %Player{id: 1234, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %ProductBacklogItem{type: :bug, size: 5},
      %ProductBacklogItem{type: :bug, size: 3}
    ]

    game_state =
      %GameState{players: [player, player2], sprint_backlog: sprint_backlog}
      |> GameBasics.play_card(player, 0, 1)

    updated_player = Enum.at(game_state.players, 0)
    backlog_item = Enum.at(game_state.sprint_backlog, 1)

    assert length(updated_player.hand) == length(player.hand) - 1
    assert updated_player.actions == 1
    assert backlog_item.size == 2
  end

  test "a player can discover technical debt" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: [%{name: :work_2, value: 1}]}
    player2 = %Player{id: 1234, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %ProductBacklogItem{type: :bug, size: 5},
      %ProductBacklogItem{size: 3}
    ]

    game_state =
      %GameState{
        players: [player, player2],
        sprint_backlog: sprint_backlog,
        work_deck: [%{type: :technical_debt}]
      }
      |> GameBasics.draw_card(player)

    updated_player = Enum.at(game_state.players, 0)
    bug_item = Enum.at(game_state.sprint_backlog, 0)
    backlog_item = Enum.at(game_state.sprint_backlog, 1)

    assert updated_player.actions == 1
    assert updated_player.actions == 1
    assert bug_item.size == 7
    assert backlog_item.size == 5
    assert length(game_state.sprint_backlog) == 3
  end

  test "estimates are affected by technical debt" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %{type: :technical_debt, size: 10},
      %{type: :technical_debt, size: 10},
      %ProductBacklogItem{type: :bug, size: 5}
    ]

    product_backlog = [
      %ProductBacklogItem{}
    ]

    game_state =
      %GameState{
        players: [player],
        sprint_backlog: sprint_backlog,
        product_backlog: product_backlog,
        work_deck: [%{type: :technical_debt}],
        sizing_deck: [%{name: :size_5, value: 5}]
      }
      |> GameBasics.estimate_feature(player)

    backlog_item = Enum.at(game_state.product_backlog, 0)

    assert backlog_item.size == 9
    assert length(backlog_item.modifiers) == 2
  end

  test "items on Sprint Backlog can also be estimated" do
    player = %Player{id: 123, role: :dev, actions: 2, hand: []}

    sprint_backlog = [
      %{type: :technical_debt, size: 10},
      %{type: :technical_debt, size: 10},
      %ProductBacklogItem{type: :maps}
    ]

    product_backlog = [
      %ProductBacklogItem{}
    ]

    game_state =
      %GameState{
        players: [player],
        sprint_backlog: sprint_backlog,
        product_backlog: product_backlog,
        sizing_deck: [%{name: :size_5, value: 5}]
      }
      |> GameBasics.estimate_feature(player)

    product_backlog_item = Enum.at(game_state.product_backlog, 0)
    sprint_backlog_item = Enum.at(game_state.sprint_backlog, 2)

    assert sprint_backlog_item.size == 9
    assert product_backlog_item.size == 0
    assert length(sprint_backlog_item.modifiers) == 2
  end
end
