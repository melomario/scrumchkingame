defmodule DecksFeatureTest do
  use ExUnit.Case
  alias GameEngine.Decks.Feature

  doctest Feature

  test "feature deck has 11 cards of each suit" do
    feature_deck = Feature.new_deck()
    assert Enum.count(feature_deck, fn card -> card.suit == :turing end) == 11
    assert Enum.count(feature_deck, fn card -> card.suit == :skip end) == 11
    assert Enum.count(feature_deck, fn card -> card.suit == :ritchie end) == 11
    assert Enum.count(feature_deck, fn card -> card.suit == :lovelace end) == 11
  end
end
