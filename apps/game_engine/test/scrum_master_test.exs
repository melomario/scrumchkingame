defmodule ScrumMasterTest do
  use ExUnit.Case

  alias GameEngine.{GameState, Player, ScrumMaster, ProductBacklogItem}

  setup do
    product_backlog =
      1..5
      |> Enum.map(fn _index -> %ProductBacklogItem{} end)

    {:ok, product_backlog: product_backlog}
  end

  test "the ScrumMaster should be the one facilitating the Sprint Planning" do
    dev_team_member = %Player{role: :dev}
    game_state = ScrumMaster.plan_sprint(%GameState{}, dev_team_member, 5, 3)
    assert game_state == {:error, "The ScrumMaster should facilitate the Sprint Planning"}
  end

  test "each sprint can only be planned once" do
    dev_team_member = %Player{role: :sm}
    game_state = ScrumMaster.plan_sprint(%GameState{planned?: true}, dev_team_member, 5, 3)
    assert game_state == {:error, "This Sprint is already planned"}
  end

  test "the sprint goal can't be bigger than the Sprint Backlog" do
    dev_team_member = %Player{role: :sm}
    game_state = ScrumMaster.plan_sprint(%GameState{}, dev_team_member, 5, 7)
    assert game_state == {:error, "The Sprint Goal should be a subset of the Sprint Backlog"}
  end

  test "we can't plan more than we have in our Product Backlog", state do
    dev_team_member = %Player{role: :sm}

    game_state =
      ScrumMaster.plan_sprint(
        %GameState{product_backlog: state.product_backlog},
        dev_team_member,
        6,
        2
      )

    assert game_state == {:error, "Your Product Backlog doesn't have that many items"}
  end

  test "the ScrumMaster can facilitate the Sprint Planning", original_state do
    dev_team_member = %Player{role: :sm}
    sprint_size = 3
    sprint_goal = 2

    game_state =
      ScrumMaster.plan_sprint(
        %GameState{product_backlog: original_state.product_backlog},
        dev_team_member,
        sprint_size,
        sprint_goal
      )

    sprint_goal_items =
      game_state.sprint_backlog
      |> Enum.count(&(&1.goal? == true))

    assert length(game_state.product_backlog) ==
             length(original_state.product_backlog) - sprint_size

    assert length(game_state.sprint_backlog) == sprint_size
    assert sprint_goal_items == sprint_goal
    assert game_state.planned? == true
  end

  test "the ScrumMaster can't remove an impediment when there's none" do
    game_state =
      %GameState{}
      |> ScrumMaster.remove_impediment(:player_1)

    assert {:error, "There's no impediment to remove"} == game_state
  end

  test "the ScrumMaster needs to spend an action to remove an impediment" do
    player = %Player{actions: 0}

    game_state =
      %GameState{impediments: 1, players: [player]}
      |> ScrumMaster.remove_impediment(player)

    assert {:error, "You don't have any actions left"} == game_state
  end

  test "the ScrumMaster can spend an action to remove an impediment" do
    player = %Player{actions: 1}
    player2 = %Player{actions: 1}

    game_state =
      %GameState{impediments: 1, players: [player, player2]}
      |> ScrumMaster.remove_impediment(player)

    updated_player = Enum.at(game_state.players, 0)

    assert game_state.impediments == 0
    assert updated_player.actions == 0
  end
end
