defmodule ShufflerTest do
  use ExUnit.Case
  alias GameEngine.Decks.Shuffler

  doctest Shuffler

  @sample_deck [
    %{name: :test, quantity: 10},
    %{name: :foo, quantity: 12},
    %{name: :bar, quantity: 15},
    %{name: :waz, quantity: 13}
  ]

  test "creates a card deck from its specification" do
    full_deck = Shuffler.unwind_deck(@sample_deck)
    assert Enum.count(full_deck) == 50
  end
end
