defmodule ProductOwnerTest do
  use ExUnit.Case

  alias GameEngine.{GameBasics, ProductOwner, GameState, Player, ProductBacklogItem}

  @business_value_deck_size 44 - 3

  setup do
    {:ok, game_pid} = GameEngine.create_game()
    GameEngine.add_player(game_pid, "mario", "mario", :sm)
    GameEngine.add_player(game_pid, "alysson", "alysson", :po)
    GameEngine.add_player(game_pid, "jacque", "jacque", :dev)
    GameEngine.add_player(game_pid, "chris", "chris", :dev)

    GameEngine.add_player(game_pid, "marcelo", "marcelo", :dev)

    {:ok, game_state: GameEngine.get_game_state(game_pid), game_pid: game_pid}
  end

  test "the product owner can discover a new feature", state do
    game_pid = state[:game_pid]
    game_state = state[:game_state]
    player_id = Enum.at(game_state.players, 0).id
    feature_deck_length = game_state.feature_deck
    backlog_size = Enum.count(game_state.product_backlog)

    game_state = GameEngine.discover_feature(game_pid, player_id)

    assert Enum.count(game_state.product_backlog) == backlog_size + 1
    assert game_state.feature_deck == feature_deck_length - 1
  end

  test "new features join the product backlog at the end of queue" do
    game_state =
      %GameState{product_backlog: [:feature1, :feature2]}
      |> ProductOwner.discover_feature(:GAME_MASTER)

    assert Enum.at(game_state.product_backlog, 0) == :feature1
    assert Enum.at(game_state.product_backlog, 1) == :feature2
  end

  test "the product owner can discover the business value of a feature", state do
    game_pid = state[:game_pid]
    game_state = state[:game_state]
    player_id = Enum.at(game_state.players, 0).id

    game_state = GameEngine.discover_feature(game_pid, player_id)

    first_unvalued_item_index =
      Enum.find_index(game_state.product_backlog, fn item -> item.value == 0 end)

    game_state = GameEngine.discover_business_value(game_pid, player_id)

    feature = Enum.at(game_state.product_backlog, first_unvalued_item_index)

    assert feature.value > 0
    assert feature.original_value > 0
    assert game_state.business_value_deck == @business_value_deck_size - 1
  end

  test "the PO can't discover the value if no feature is without value" do
    game_state =
      %GameState{product_backlog: []}
      |> ProductOwner.discover_business_value(:GAME_MASTER)

    assert {:error, "There's no feature without value"} = game_state
  end

  test "only the Product Owner can change the Product Backlog order" do
    player = %Player{role: :dev, actions: 2, id: 123, name: "teste"}

    game_state =
      %GameState{product_backlog: [:item1, :item2, :item3, :item4], players: [player]}
      |> ProductOwner.change_item_priority(player, 2, 0)

    assert {:error, "Only the PO can reorder the Product Backlog"} == game_state
  end

  test "a Product Owner can increase the priority of an item" do
    player = %Player{role: :po, actions: 2, id: 123, name: "teste"}

    game_state =
      %GameState{
        planned?: true,
        product_backlog: [:item1, :item2, :item3, :item4],
        players: [player]
      }
      |> ProductOwner.change_item_priority(player, 2, 0)

    assert game_state.product_backlog == [:item3, :item1, :item2, :item4]
  end

  test "the PO can't reorder the backlog if there's no available actions" do
    player = %Player{role: :po, actions: 0, id: 123, name: "teste"}

    game_state =
      %GameState{product_backlog: [:item1, :item2, :item3, :item4], players: [player]}
      |> ProductOwner.change_item_priority(player, 2, 0)

    assert {:error, "You played all your actions for today"} == game_state
  end

  test "a Product Owner can decrease the priority of an item" do
    player = %Player{role: :po, actions: 2, id: 123, name: "teste"}

    game_state =
      %GameState{
        planned?: true,
        product_backlog: [:item1, :item2, :item3, :item4],
        players: [player]
      }
      |> ProductOwner.change_item_priority(player, 0, 3)

    assert game_state.product_backlog == [:item2, :item3, :item4, :item1]
  end

  test "items on Sprint Backlog can also have its value discovered" do
    player = %Player{id: 123, role: :po, actions: 2, hand: []}

    sprint_backlog = [
      %ProductBacklogItem{type: :technical_debt, original_value: 10},
      %ProductBacklogItem{type: :bug, original_value: 50},
      %ProductBacklogItem{type: :maps}
    ]

    product_backlog = [
      %ProductBacklogItem{}
    ]

    game_state =
      %GameState{
        players: [player],
        sprint_backlog: sprint_backlog,
        product_backlog: product_backlog,
        business_value_deck: [%{name: :bv_100, value: 100}]
      }
      |> GameBasics.discover_business_value(player)

    product_backlog_item = Enum.at(game_state.product_backlog, 0)
    sprint_backlog_item = Enum.at(game_state.sprint_backlog, 2)

    assert sprint_backlog_item.value == 100
    assert sprint_backlog_item.original_value == 100
    assert product_backlog_item.value == 0
  end
end
