1 - Comecei o projeto criando este walkthrough. 
2 - Defini a interface do GameEngine utilizando `defdelegate`.
3 - Criei um status inicial para o jogo em `GameStatus`
4 - Escrevi o teste para `create_game` e fiz o esqueleto da função
5 - Instalei o `ex_unit_notifier`
6 - Adicionei o `mix-test.watch` para automatizar o TDD
7 - Criei o deck **WORK**
8 - Escrevi o teste e implementei o método `unwind_deck`
10 - Escrevi o teste para validar os naipes do **feature deck**
11 - Criei os decks restantes e os adicionei no estado inicial do jogo em `Engine.create_game/0`
12 - Criei o teste e a função para adicionar novos jogadores
13 - Adicionei a biblioteca UUID para gerar IDs para os jogadores
14 - Alterei o nome do módulo GameStatus para GameState
15 - Criei o GameServer para poder ter mais jogos em paralelo
16 - Criei o teste e corrigi o bug onde jogadores podiam ter o mesmo id
17 - Transformei em uma aplicação e adicionei um supervisor dinâmico para os jogos
18 - Escrevi os testes para a função `draw_card`
19 - Criei duas funções privadas: `use_one_action` e `update_player`
20 - Implementei a função draw_card
21 - Criei o teste para a função que adiciona um novo item ao product_backlog
22 - Implementei a função que descobre uma feature do Product Backlog
23 - Extraí a funcão `get_player`em `GameServer`
24 - Criei a função `format_response` para começar a tratar jogadas inválidas
25 - Criei a Estrutura `ProductBacklogItem`
26 - Criei a função `get_summary` para evitar de enviar o conteúdo dos decks para o cliente
27 - Criei testes e implementei a função `discover_business_value`
28 - Criei testes e implementei a função `estimate_feature`

TODO:

- Criar backlog inicial
- Terminar o dia automaticamente
- Implementar compra de cartas que são jogadas imediatamente
- Pensar num mecanismo de Planning
- Pensar num mecanismo de Review
